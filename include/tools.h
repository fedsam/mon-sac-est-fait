/**
* @file tools.h
* @author Grp SacEstFait
* @brief Fonctions outils
*/

#ifndef TOOLS_H
#define TOOLS_H
#include "./../include/instance.h"
#include "./../include/item.h"
/**
 * @brief Détermine l'indice de la valeur maximum d'un tableau donné
 * @param array 
 * @param length 
 * @return int 
 */
int posMax(float *array, int length);

/**
 * @brief Affiche la valeur des items
 * 
 * @param itemList 
 * @param nbItems 
 */
void printItemValue(Item **itemList, int nbItems);

/**
 * @brief Vérifie la solution
 * 
 * @param instance 
 * @param itemList 
 * @return int 
 */
int checkSolution(Instance *instance,Item** itemList);

/**
 * @brief Donne la valeur des objets dans le sac
 * 
 * @param itemList 
 * @param nbItems 
 * @return int 
 */
int objectiveFunction(Item ** itemList, int nbItems);


/**
 * @brief Vide le sac
 * @param instance 
 */
void emptyBag(Instance * instance);
   

#endif