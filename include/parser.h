/**
 * @file parser.h
 * @author Grp SacEstFait
 * @brief Permet de gérer le parser
 */

#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include "./../include/instance.h"

typedef struct {
	int nbInstance;
	Instance **instanceArray;
	FileIterator *fIterator;
} Parser;

/**
 * @brief Crée un parser
 * @param file 
 * @return Parser* 
 */
Parser* Parser_new(FILE* file);

/**
 * @brief Initialise un parser
 * @param parser 
 * @param file 
 * @return int 
 */
int Parser_init(Parser * parser,FILE* file);

/**
 * @brief Libère les éléments de la structure parser
 * @param parser 
 */
void Parser_finalize(Parser* parser);

/**
 * @brief Supprime le parser
 * @param parser 
 */
void Parser_delete(Parser* parser);

#endif