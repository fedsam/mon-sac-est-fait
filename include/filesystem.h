/**
 * @file filesystem.h
 * @author Grp SacEstFait
 * @brief Fonctions permettant de lire le fichiers et ses données
 */

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#define BUFFER_SIZE 1024

typedef struct {
	FILE *file;
	char *current;
	
} FileIterator;

/**
 * @brief Crée un fileIterator
 * @param file 
 * @return FileIterator* 
 */
FileIterator* FileIterator_new(FILE *file);

/**
 * @brief initialise un fileIterator
 * @param fileIterator 
 * @param file 
 * @return int 
 */
int FileIterator_init(FileIterator *fileIterator , FILE *file);

/**
 * @brief Supprime un fileIterator
 * @param fileIterator 
 */
void FileIterator_delete(FileIterator *fileIterator);

/**
 * @brief Libère les éléments de la structure fileIterator
 * @param fileIterator 
 */
void FileIterator_finalize(FileIterator *fileIterator);

/**
 * @brief Vérifie s'il existe une ligne suivante
 * @param fileIterator 
 * @return int 
 */
int FileIterator_hasNext(FileIterator *fileIterator);

/**
 * @brief Passe à la ligne suivante
 * @param fileIterator 
 */
void FileIterator_next(FileIterator *fileIterator);

/**
 * @brief Passe à la valeur suivante
 * @param fileIterator 
 * @return int 
 */
int FileIterator_nextInt(FileIterator *fileIterator);

/**
 * @brief Récupère la ligne courante
 * @param fileIterator 
 * @return const char* 
 */
const char* FileIterator_get(FileIterator *fileIterator);

/**
 * @brief Récupère le nombre d'instance dans le fichier
 * @param fileIterator 
 * @return int 
 */
int FileIterator_getNbInstance(FileIterator *fileIterator);

/**
 * @brief Récupère le nombre d'items par instance
 * @param fileiterator 
 * @return int 
 */
int FileIterator_getNbItems(FileIterator *fileiterator);

/**
 * @brief Récupère le nombre de dimensions par instance
 * @param fileIterator 
 * @return int 
 */
int FileIterator_getNbDimensions(FileIterator *fileIterator);

#endif