/**
 * @file interface.h
 * @author Grp SacEstFait
 * @brief Fonctions permettant de gérer l'affichage
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdio.h>
#include <stdlib.h>
#include "./../include/heuristic.h"
#include "./../include/parser.h"
#include "./../include/research.h"
#include "./../include/sort.h"

/**
 * @brief Affiche le choix de tri
 */
void printChoiceOfSorting();

/**
 * @brief Lance la recherche locale si elle est choisie
 * @param parser 
 * @return unsigned int 
 */
unsigned int launchLocalResearch(Parser *parser);

/**
 * @brief Lance l'interface
 * @param parser 
 */
void interface(Parser *parser);

#endif