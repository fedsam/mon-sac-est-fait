/**
 * @file research.h
 * @author Grp SacEstFait
 * @brief Recherche locale
 */

#ifndef RESEARCH_H
#define RESEARCH_H

#include "./../include/item.h"
#include "./../include/parser.h"
#include "./../include/tools.h"

/**
 * @brief Recherche locale
 * 
 * @param parser 
 * @param t 
 * @return Item** 
 */
Item** localResearch(Parser * parser,int t);

#endif