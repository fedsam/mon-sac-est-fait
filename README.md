<img align="right" src="./Resources/icon.png">

# Mon sac est fait (Projet Tutoré 2)

Ce projet a pour but de résoudre le problème du sac à dos de la manière la plus optimisée possible.

## Installation et lancement du projet

```bash
$ git clone https://gitlab.com/vincent.bastide/mon-sac-est-fait.git
$ make run
```

## Options du Makefile

Lancer le projet :

```bash
$ make run
```

Lancer un test Valgrind :
```bash
$ make valgrind
```

Nettoyer les fichiers objets :

```bash
$ make clean
```

Nettoyer entièrement le projet :

```bash
$ make mrproper
```

## Docker

Pour utiliser le container Docker il faut :

Créer l'image à partir du __Dockerfile__ 

```bash
$ docker build -t imagename .
```

Créer un container en montant le répertoire courant dans le dossier __/app__ du container

```bash
$ docker run -ti \
    --name containername \
    -v $(pwd):/app \
    nomimage
```

Pour relancer un container et se placer dedans :

```bash
$ docker container restart containername
$ docker container exec -it containername bash
```