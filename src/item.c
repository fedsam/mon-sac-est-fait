#include "./../include/item.h"

Item* Item_new(int nbDimensions){
    Item *newItem = (Item*) malloc(sizeof(Item));
    if (newItem) {
        if (Item_init(newItem,nbDimensions)) {
            free(newItem);
            newItem = NULL;
        } 
    }
    return newItem;
}
int Item_init(Item *item, int nbDimensions) {
    item->value = 0;
    item->included = 0;
    item->weight = calloc(nbDimensions, sizeof(int));
	return 0;
}

void Item_finalize(Item *item) {
    if (item) {
        free(item->weight);
    }
}

void Item_delete(Item *item) {
    if (item) {
        Item_finalize(item);
        free(item);
    }
}

int Item_getValue(Item *item) {
    return item->value;
}

int Item_getWeight(Item *item, int dimensionNumber) {
    return 0;
}

void setIncluded(Item *item, int boolean) {
    item->included = boolean;
}
