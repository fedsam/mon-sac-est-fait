#include "./../include/tools.h"

int posMax(float * array, int length){
    float max = array[0];
    int position = 0;
    for(int i = 1; i < length ; i++){
        if(array[i] > max){
            max = array[i];
            position = i;
        }
    }
    return position;
}


void printItemValue(Item **itemList, int nbItems) {
    for(int i = 0 ; i < nbItems ; i++){
        printf("Item %d : %d\n",i,itemList[i]->value);
    }
}

// codage direct
int checkSolution(Instance *instance, Item ** itemList) {
    int nbDimensions = Instance_getNbDimensions(instance);
    int nbItems = Instance_getNbItems(instance);
    int resTab[nbDimensions];

    for (int i = 0; i < nbDimensions; i++){
        int res = 0;
        for(int j = 0 ; j < nbItems;j++){
            if(itemList[j]->included == 1){
                res += itemList[j]->weight[i];
            }
        }
        resTab[i]= res;
    }
    for(int i = 0 ; i < nbDimensions;i++){
       //printf("Valeur %d de tab : %d\n",i,resTab[i]);
        if(resTab[i] > instance->maxWeight[i]){
            return 0;
        }
    }
    return 1;
}

int objectiveFunction(Item ** itemList,int nbItems) {
    int res = 0;
    for (int i = 0; i < nbItems; i++){
        if (itemList[i]->included == 1) {
            res += itemList[i]->value;
        }
    }
    return res;
}
void emptyBag(Instance * instance){
    int nbItems = Instance_getNbItems(instance);

    for (int i = 0; i < nbItems ; i++) {
        instance->itemList[i]->included = 0;
    }
}