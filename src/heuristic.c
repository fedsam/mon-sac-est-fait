#include "./../include/heuristic.h"

Item ** fillBag(Parser *parser, Item **sortedItemList, int dynamical, int idInstance) {
    int nbDimensions = Instance_getNbDimensions(parser->instanceArray[idInstance]);
    int nbItems = Instance_getNbItems(parser->instanceArray[idInstance]);
    int restab[nbDimensions];
    
    for (int i = 0; i < nbDimensions ; i++) {
        restab[i] = 0;
    }

    int flag = 0;
    for (int i = 0; i < nbItems; i++) {
        for (int j = 0; j < nbDimensions; j++) {
            if (restab[j] + sortedItemList[i]->weight[j] > parser->instanceArray[idInstance]->maxWeight[j]) {
                flag = 1;
            }
        }
        if (flag == 0) {
            for (int j = 0; j < nbDimensions; j++) {
                restab[j] += sortedItemList[i]->weight[j];
            }

            sortedItemList[i]->included = 1;
            if (dynamical == 1) {
                sortedItemList = ratioOrderCritical(parser->instanceArray[idInstance]);
            }
        }
        flag = 0;
    }
   
    return sortedItemList;
}
