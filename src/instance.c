#include "./../include/instance.h"

Instance* Instance_new(int nbItems, int nbDimensions, FileIterator *fileIterator) {
	Instance *newInstance = (Instance*)malloc(sizeof(Instance));
    Timer timer;

    // Démarrage Timer
    Timer_start(&timer);
    if (newInstance){
        if (Instance_init(newInstance,nbItems,nbDimensions,fileIterator)) {
            free(newInstance);
            newInstance = NULL;
        } 
    }
    // Arrêt Timer
    Timer_stop(&timer);

    newInstance->initTime = Timer_getElapsedTime(&timer);
    return newInstance;
}

int Instance_init(Instance* instance ,int nbItems ,int nbDimensions,FileIterator* fileIterator){
	if (instance) {
        //Ignore the first line with 0 and 1
        for (int i = 0; i < nbItems + 2; i++) {
            FileIterator_nextInt(fileIterator);
        }

        int *maxWeight = (int*) malloc(sizeof(int) * nbDimensions);
		Item **newItem = (Item**) malloc(sizeof(Item*) * nbItems);

        for(int i = 0; i < nbItems; i++){
            newItem[i] = Item_new(nbDimensions);
        }

		for(int i = 0; i < nbItems; i++){
            newItem[i]->value = FileIterator_nextInt(fileIterator);
        }

        for(int j = 0 ; j < nbDimensions; j++){
            for(int i = 0; i < nbItems; i++){
                newItem[i]->weight[j] = FileIterator_nextInt(fileIterator);
            }
        }
      
        for(int i = 0 ; i < nbDimensions; i++){
           maxWeight[i] = FileIterator_nextInt(fileIterator);
           
        }
        for(int i = 1 ; i <= 2; i++){
            FileIterator_nextInt(fileIterator);
        }

		instance->itemList = newItem;
		instance->nbItems= nbItems;
		instance->nbDimensions = nbDimensions;
        instance->maxWeight = maxWeight;
		return 0;
	}
	return 1;
}

void Instance_finalize(Instance* instance){
    if (instance) {
        free(instance->maxWeight);
        for (int i = 0; i < instance->nbItems; i++) {
            Item_delete(instance->itemList[i]);
        }
        free(instance->itemList);
    }
}

void Instance_delete(Instance* instance){
    if (instance) {
        Instance_finalize(instance);
        free(instance);
    }
}

int Instance_getNbItems(Instance* instance){
	return instance->nbItems;
}

int Instance_getNbDimensions(Instance* instance){
	return instance->nbDimensions;
}

Item ** Instance_copyItemList(Item **itemList,int nbItems, int nbDimensions){
    Item **tmpItem = (Item**)malloc(sizeof(Item*)*nbItems);
    for(int i = 0; i < nbItems; i++){
        tmpItem[i] = Item_new(nbDimensions);
        tmpItem[i]->included = itemList[i]->included;
      
        for(int j = 0; j < nbDimensions; j++){
            tmpItem[i]->weight[j] = itemList[i]->weight[j];
        }
        tmpItem[i]->value = itemList[i]->value;
    }
    return tmpItem;
}
void Instance_ItemList_Delete(Item** itemList,int nbItems){
    for(int i = 0 ; i < nbItems; i++){
        Item_delete(itemList[i]);
    }
    free(itemList);
}
