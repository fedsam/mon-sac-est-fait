#include "./../include/timer.h"

void Timer_start(Timer *timer) {
	timer->startTime = clock();
}

void Timer_stop(Timer *timer) {
	timer->stopTime = clock();
}

int Timer_getElapsedTime(Timer *timer) {
	return (unsigned int) ((timer->stopTime - timer->startTime) / CLOCKS_PER_MS); 
}